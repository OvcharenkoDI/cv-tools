from ultralytics import YOLO
import cv2
import supervision as sv
import os
import numpy as np
import copy

MODEL_PATH = 'model'
VIDEOS_PATH = 'videos'
TEMP_PATH = 'temp'


def get_model():
    file_names = [f for f in os.listdir(MODEL_PATH) if os.path.isfile(os.path.join(MODEL_PATH, f))]
    return YOLO(MODEL_PATH + '/' + file_names[0])


def get_video_path():
    file_names = [f for f in os.listdir(VIDEOS_PATH) if os.path.isfile(os.path.join(VIDEOS_PATH, f))]

    if len(file_names) == 0:
        raise Exception("video is empty")

    if len(file_names) == 1:
        return VIDEOS_PATH + '/' + file_names[0]

    print('Видео:')
    for n, item in enumerate(file_names):
        print(f'{n + 1}: {item}')
    n = int(input('Выбери пункт: ')) - 1

    return "videos/" + file_names[n]


def custom_detect():
    model = get_model()
    cap = cv2.VideoCapture(get_video_path())

    while cap.isOpened():
        success, frame = cap.read()
        if not success:
            break

        result = model(frame, agnostic_nms=True)[0]

        detections = sv.Detections.from_yolov8(result)

        box_annotator = sv.BoxAnnotator(thickness=2, text_thickness=2, text_scale=1)
        labels = [
            f"{model.model.names[class_id]} {confidence:0.2f}"
            for xyxy, mask, confidence, class_id, tracker_id
            in detections
        ]

        frame = box_annotator.annotate(
            scene=frame,
            detections=detections,
            labels=labels
        )

        cv2.imshow('Result', frame)

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break


def custom_detect_with_filet():
    model = get_model()
    cap = cv2.VideoCapture(get_video_path())

    print("1. Выводить видео без преобразований")
    print("2. Выводить видео с преобразованиями")
    isFilteredFinalVideo = int(input('Выбери пункт: ')) != 1

    while cap.isOpened():

        # sharp_filter = np.array([[-1, -1, -1], [-1, 16, -1], [-1, -1, -1]])
        # frame = cv2.resize(frame, (640, 640))
        # frame =cv2.filter2D(frame, ddepth=-1, kernel=sharp_filter)
        # frame = cv2.blur(frame, (7, 7))
        # frame = cv2.GaussianBlur(frame, (15, 15), 0)
        # frame =cv2.dilate(frame, kernel, iterations=1)
        # frame = cv2.Sobel(frame, ddepth=cv2.CV_64F, dx=1, dy=1, ksize=5)
        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        success, frame = cap.read()
        if not success:
            break

        kernel = np.ones((5, 5), np.uint8)

        filter_frame = cv2.medianBlur(frame, 7)
        filter_frame = cv2.cvtColor(filter_frame, cv2.COLOR_BGR2GRAY)
        filter_frame = cv2.erode(filter_frame, kernel, iterations=1)

        result = model(filter_frame, agnostic_nms=True)[0]

        detections = sv.Detections.from_yolov8(result)

        box_annotator = sv.BoxAnnotator(thickness=2, text_thickness=2, text_scale=1)
        labels = [
            f"{model.model.names[class_id]} {confidence:0.2f}"
            for xyxy, mask, confidence, class_id, tracker_id
            in detections
        ]

        if isFilteredFinalVideo:
            result_frame = filter_frame
        else:
            result_frame = frame

        frame = box_annotator.annotate(
            scene=result_frame,
            detections=detections,
            labels=labels
        )

        cv2.imshow('Result', result_frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


def custom_detect_with_motion():
    model = get_model()
    cap = cv2.VideoCapture(get_video_path())

    _, motion_frame = cap.read()


    print("1. Отмечать движение сегментами")
    print("2. Отмечать движение боксами")
    isBox = int(input('Выбери пункт: ')) != 1

    while cap.isOpened():
        success, frame = cap.read()
        start_frame = copy.copy(frame)

        if not success:
            break

        diff = cv2.absdiff(motion_frame, frame)
        gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)  # перевод кадров в черно-белую градацию
        blur = cv2.GaussianBlur(gray, (5, 5), 0)  # фильтрация лишних контуров
        _, thresh = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY)  # метод для выделения кромки объекта белым цветом
        dilated = cv2.dilate(thresh, None,iterations=3)  # данный метод противоположен методу erosion(), т.е. эрозии объекта, и расширяет выделенную на предыдущем этапе област
        countries, _ = cv2.findContours(dilated, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)  # нахождение массива контурных точек

        filter_countries = []
        for contour in countries:
            if cv2.contourArea(contour) > 100:
                filter_countries.append(contour)

        result = model(frame, agnostic_nms=True)[0]

        detections = sv.Detections.from_yolov8(result)
        box_annotator = sv.BoxAnnotator(thickness=2, text_thickness=2, text_scale=1)
        labels = [
            f"{model.model.names[class_id]} {confidence:0.2f}"
            for xyxy, mask, confidence, class_id, tracker_id
            in detections
        ]
        frame = box_annotator.annotate(
            scene=frame,
            detections=detections,
            labels=labels
        )

        if isBox:
            for contour in filter_countries:
                (x, y, w, h) = cv2.boundingRect(contour)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        else:
            cv2.drawContours(frame, filter_countries, -1, (255, 0,), 2)


        cv2.imshow('Result', frame)
        motion_frame = start_frame
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break



def motion_tracking():
    cap = cv2.VideoCapture(get_video_path())
    background_subtractor = cv2.bgsegm.createBackgroundSubtractorMOG()

    # получаем ширину и высоту кадра
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    print(length, fps, (width, height))

    # создаем объект класса cv2.VideoWriter
    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    out = cv2.VideoWriter(TEMP_PATH + "/output.mp4", fourcc, fps, (width, height))

    first_iteration_indicator = 1

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        # If first frame
        if first_iteration_indicator == 1:
            first_frame = copy.deepcopy(frame)
            height, width = frame.shape[:2]
            accum_image = np.zeros((height, width), np.uint8)
            first_iteration_indicator = 0
        else:
            filter = background_subtractor.apply(frame)  # remove the background
            filter = cv2.GaussianBlur(filter, (5, 5), 0)

            threshold = 2
            maxValue = 2
            ret, th1 = cv2.threshold(filter, threshold, maxValue, cv2.THRESH_BINARY)
            th1[th1 < 2] = 0  # Пороговая фильтрация

            kernel = np.ones((5, 5), np.uint8)
            th1 = cv2.erode(th1, kernel, iterations=2)
            th1 = cv2.dilate(th1, kernel, iterations=1)

            # add to the accumulated image
            accum_image = cv2.add(accum_image, th1)
            cv2.imwrite(TEMP_PATH + '/mask.jpg', accum_image)

            color_image_video = cv2.applyColorMap(accum_image, cv2.COLORMAP_HOT)
            # color_image_video = cv2.applyColorMap(accum_image, cv2.COLORMAP_SUMMER)
            video_frame = cv2.addWeighted(frame, 0.7, color_image_video, 0.7, 0)
            accum_image = cv2.GaussianBlur(accum_image, (5, 5), 0)
            kernel = np.ones((2, 2), np.uint8)
            accum_image = cv2.erode(accum_image, kernel, iterations=1)
            accum_image[accum_image < 2] = 0  # Пороговая фильтрация

            # Find contours
            cnts = cv2.findContours(accum_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            cnts = cnts[0] if len(cnts) == 2 else cnts[1]

            # Создаем список для хранения прямоугольников, ограничивающих контуры
            rects = []
            for c in cnts:
                x, y, w, h = cv2.boundingRect(c)
                rects.append((x, y, w, h))
                cv2.rectangle(video_frame, (x, y), (x + w, y + h), (36, 255, 12), 2)

            out.write(video_frame)
            cv2.imshow('frame', video_frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    color_image = cv2.applyColorMap(accum_image, cv2.COLORMAP_HOT)
    result_overlay = cv2.addWeighted(first_frame, 0.7, color_image, 0.7, 0)

    # save the final heatmap
    cv2.imwrite('diff-overlay.jpg', result_overlay)

    # cleanup
    cap.release()
    out.release()
    cv2.destroyAllWindows()


def yolo_detect():
    model = get_model()
    model.track(source=get_video_path(), show=True)


def train_model():
    model = YOLO('yolov8n.pt')

    results = model.train(
        data='dataset/data.yaml',
        imgsz=640,
        epochs=50,
        batch=-1,
        name='yolov8n_custom',
    )


def main():
    while True:
        print("1. custom_detect")
        print("2. custom_detect_with_morf")
        print("3. yolo_detect")
        print("4. motion_tracking by Ruslan")
        print("5. train_model")
        print("6. custom_detect_with_motion")

        cmd = input("Выбери пункт: ")

        if cmd == "1":
            custom_detect()
            break
        elif cmd == "2":
            custom_detect_with_filet()
            break
        elif cmd == "3":
            yolo_detect()
            break
        elif cmd == "4":
            motion_tracking()
            break
        elif cmd == "5":
            train_model()
            break
        elif cmd == "6":
            custom_detect_with_motion()
            break
        else:
            print("Error!")


if __name__ == '__main__':
    main()
